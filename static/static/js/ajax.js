$(function() {
	
	$('#search').keyup(function() {
		$.ajax({
			type: 'POST',
			url: '/testblog/search/',
			data: {
				'search-text' : $('#search').val(),
			},
			success: searchSuccess,
			dataType: 'html'
		});
	});
});

function searchSuccess(data, textStatus, jqXHR) {
	$('#search-results').html(data);
}