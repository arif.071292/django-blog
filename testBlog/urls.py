import os
from django.conf import settings
from django.contrib import admin
from django.conf.urls.static import static
from django.conf.urls import patterns, include, url

urlpatterns = patterns('',

    # admin site
    url(r'^admin/', include(admin.site.urls)),

    # homes url
    url(r'^testblog/home/', 'homes.views.home', name='home'),
    url(r'^testblog/tag/(?P<category>\D+)/', 'homes.views.category_post', name='tag'),
    url(r'^testblog/new_post/', 'homes.views.new_post', name='new_article'),
    #url(r'^testblog/search/', 'homes.views.search'),
    url(r'^testblog/search_result/page/(?P<page>\d+)/', 'homes.views.search_result', name='search_result'),
    url(r'^testblog/article/(?P<post_id>\d+)/', 'homes.views.view_post', name='article'),
    url(r'^testblog/article/edit/(?P<post_id>\d+)/', 'homes.views.edit_post', name='edit_article'),
    url(r'^testblog/delete_post/(?P<post_id>\d+)/', 'homes.views.delete_post', name='delete_article'),
    url(r'^testblog/pin_post/(?P<post_id>\d+)/', 'homes.views.pin_post', name='pin_post'),
    url(r'^testblog/unpin_post/(?P<post_id>\d+)/', 'homes.views.unpin_post', name='unpin_post'),

    # authorize url
    url(r'^testblog/signin/', 'authorize.views.login', name='login'),
    url(r'^testblog/login/verify/', 'authorize.views.verify_login', name='verify'),
    url(r'^testblog/logout/', 'authorize.views.logout', name='logout'),
    url(r'^testblog/invalid/', 'authorize.views.invalid_login', name='invalid'),
    url(r'^testblog/signup/', 'authorize.views.signup', name='signup'),
    url(r'^testblog/signup_success/', 'authorize.views.signup_success', name='singup_success'),

    # profiles url
    url(r'^testblog/profile/edit/(?P<username>\w+)/', 'profiles.views.edit_profile', name='edit_profile'),
    url(r'^testblog/profile/(?P<username>\w+)/', 'profiles.views.profile', name='profile'),
    url(r'^testblog/change_password/', 'profiles.views.change_password', name='change_password'),
    url(r'^testblog/about/', 'profiles.views.about', name='about'),

    # start page
    url(r'^$', 'homes.views.welcome'),
)

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL,
                          document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)