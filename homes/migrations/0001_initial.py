# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Article',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=300)),
                ('writer', models.CharField(max_length=120)),
                ('body', models.TextField()),
                ('public', models.BooleanField(default=True)),
                ('category', models.CharField(max_length=200, db_index=True)),
                ('pinned', models.BooleanField(default=False)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
            ],
            options={
                'ordering': ['-pinned', '-created'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Article_Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('category', models.CharField(unique=True, max_length=200)),
                ('created', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'ordering': ['category'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Article_Comment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('comment', models.CharField(max_length=1200)),
                ('commentor', models.CharField(max_length=120)),
                ('post_id', models.IntegerField(db_index=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'ordering': ['post_id'],
            },
            bases=(models.Model,),
        ),
    ]
