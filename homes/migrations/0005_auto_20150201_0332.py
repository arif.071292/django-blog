# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('homes', '0004_article_is_acive'),
    ]

    operations = [
        migrations.RenameField(
            model_name='article',
            old_name='is_acive',
            new_name='is_active',
        ),
    ]
