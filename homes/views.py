from django.shortcuts import render_to_response
from django.template import RequestContext
from django.shortcuts import HttpResponseRedirect
from django.core.context_processors import csrf
from models import Article
from models import Article_Comment
from models import Article_Category
from forms import ArticleForm
from forms import Article_CommentForm

# Create your views here.

def count_categoryPost():
	list1 = []
	list2 = []

	categoy = Article_Category.objects.all()

	for item in categoy:
		list1.append(item)

	for item in list1:
		count = Article.objects.filter(category=item).count()
		list2.append(count)

	return dict(zip(list1,list2))

def welcome(request):
	return HttpResponseRedirect('/testblog/home/')

def home(request):
	login = False
	session_name = ''

	if 'name' in request.session:
		login = True
		session_name = request.session['name']

	tag = count_categoryPost()
	articles = Article.objects.filter(private=False,is_active=True)
	total = Article.objects.all().count()

	return render_to_response('blog_home.html', locals(), context_instance=RequestContext(request))

def search(request):
	search_text = ''
	if request.POST:
		keyword = request.POST.get('search_text', '')

	articles = Article.objects.filter(title__icontains=keyword,is_active=True)
	return render_to_response('ajax_search.html',locals())
	
def search_result(request,page = 1):
	login = False
	session_name = ''
	keyword = ''

	if request.POST:
		keyword = request.POST.get('search', '')

	if 'name' in request.session:
		login = True
		session_name = request.session['name']

	tag = count_categoryPost()
	articles = Article.objects.filter(title__icontains = keyword, private=False,is_active=True)

	return render_to_response('search_result.html', locals(), context_instance=RequestContext(request))

def category_post(request,category = ''):
	login = False
	session_name = ''

	if 'name' in request.session:
		login = True
		session_name = request.session['name']

	tag = count_categoryPost()
	articles = Article.objects.filter(category=category,private=False)
	total = Article.objects.all().count()

	return render_to_response('selective_post.html', locals(), context_instance=RequestContext(request))

def new_post(request):

	login = False
	session_name = ''

	if 'name' in request.session:
		login = True
		session_name = request.session['name']

			
		if request.POST:
			form = ArticleForm(request.POST)

			if form.is_valid():
				form.save()

				title = request.POST.get("title", "")
				writer = request.POST.get("writer", "")

				if request.FILES.get("image"):
					article = Article.objects.get(title = title, writer = writer)
					article.image = request.FILES.get("image")
					article.save()

			address = '/testblog/home/'
			return HttpResponseRedirect(address)

		tag = count_categoryPost()
		total = Article.objects.all().count()

		return render_to_response('new_post.html', locals(), context_instance=RequestContext(request))

	return HttpResponseRedirect('/testblog/home/')

def view_post(request, post_id = 1):
	login = False
	session_name = ''

	if request.POST:
		form = Article_CommentForm(request.POST)
		if form.is_valid():
			form.save()

	args = {}
	args.update(csrf(request))
	args['form'] = Article_CommentForm()

	if 'name' in request.session:
		login = True
		session_name = request.session['name']

	tag = count_categoryPost()
	articles = Article.objects.filter(id = post_id, private=False,is_active=True)
	comments = Article_Comment.objects.filter(post_id = post_id)
	total = Article.objects.all().count()

	return render_to_response('blog_post.html', locals(), context_instance=RequestContext(request))

def edit_post(request, post_id = 1):
	addr = "/testblog/article/"+str(post_id)+"/"
	login = False
	session_name = ''

	if 'name' in request.session:
		session_name = request.session['name']

		try:
			article = Article.objects.get(id=post_id)
			if session_name == article.writer:
				if request.POST:
					article.title = request.POST.get('title', '')
					article.writer = session_name
					article.category = request.POST.get('category', '')
					article.body = request.POST.get('body', '')
					article.private = request.POST.get('private', '')
					if request.FILES.get("image"):
						article.image = request.FILES.get("image")

					article.save()

					return HttpResponseRedirect(addr)
			else:
				return HttpResponseRedirect(addr)
				
		except:
			return HttpResponseRedirect(addr)

		tag = count_categoryPost()
		total = Article.objects.all().count()

		return render_to_response('edit_post.html', locals(), context_instance=RequestContext(request))

	return HttpResponseRedirect(addr)

def delete_post(request, post_id = 1):
	name = ''
	address = '/testblog/home/'

	if 'name' in request.session:
		name = request.session['name']

		try:
			article = Article.objects.get(id=post_id,is_active=True)
		except:
			return HttpResponseRedirect(address)

		if name == 'admin' or name == article.writer:
			article.is_active = False
			article.save()

	return HttpResponseRedirect(address)

def pin_post(request, post_id = 1):
	name = ''
	address = '/testblog/article/'+str(post_id)+'/'

	if 'name' in request.session:
		name = request.session['name']

		try:
			article = Article.objects.get(id=post_id,is_active=True)
		except:
			return HttpResponseRedirect('/testblog/home/')

		if name == 'admin':
			article.pinned = True
			article.save()

	return HttpResponseRedirect(address)

def unpin_post(request, post_id = 1):
	name = ''
	address = '/testblog/article/'+str(post_id)+'/'

	if 'name' in request.session:
		name = request.session['name']

		try:
			article = Article.objects.get(id=post_id,is_active=True)
		except:
			return HttpResponseRedirect('/testblog/home/')

		if name == 'admin':
			article.pinned = False
			article.save()

	return HttpResponseRedirect(address)