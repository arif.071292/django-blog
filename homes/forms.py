from django.forms import ModelForm

from models import Article
from models import Article_Comment

class ArticleForm(ModelForm):
	
	class Meta:
		model = Article
		fields = ['title','writer','body','private','category','image']

class Article_CommentForm(ModelForm):
	
	class Meta:
		model = Article_Comment
		fields = ['comment','commentor','post_id']